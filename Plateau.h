#pragma once
#include "De.h"

class Plateau {
  De *des;
  public:
    Plateau();
    void tourneDes();
    int getValeur(int numDe);
    void bloquerDe(int numDe);
    De* getDes();
    int nbPoints();
};